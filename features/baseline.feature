# language: fr
Fonctionnalité : Gestion automatisée et intégrée des actions à pilotage national (APN)
  En tant que DGESCO,
  Je veux automatiser la gestion et le suivi des APN,
  Afin de réduire les erreurs de saisie, faciliter le suivi et améliorer l'intégration avec les outils existants.

  Scénario : Attribuer et suivre les ressources APN de manière centralisée
    Étant donné que les bureaux DGESCO B1-1 et B1-2 attribuent les ressources APN en début d’année scolaire,
    Et que cette attribution est actuellement gérée en partie manuellement,
    Quand DGESCO B1-1 ou B1-2 attribue les ressources APN,
    Alors le logiciel doit permettre de saisir cette attribution directement dans l'outil,
    Et envoyer une notification automatique aux bureaux bénéficiaires (ex : DGESCO A1, A2, C1) via le système intégré.

  Scénario : Suivi des attributions et consommation des ressources APN
    Étant donné que les bureaux bénéficiaires doivent suivre la consommation des ressources APN,
    Et que les données sont actuellement éparpillées et sujettes à des erreurs,
    Quand un bureau bénéficiaire consulte l’état des attributions,
    Alors le logiciel doit afficher une vue consolidée des consommations par académie,
    Et afficher une vue consolidée des consommations par bureau,
    Et permettre l’exportation des données en format Excel ou CSV pour une analyse plus approfondie.

  Scénario : Réajustement des attributions en cas de changement de besoins
    Étant donné que les besoins des bureaux bénéficiaires d'APN peuvent évoluer en cours d’année,
    Et que les réajustements sont actuellement longs et sujets à erreurs,
    Quand un bureau bénéficiaires d'APN identifie un besoin de réajustement,
    Alors le logiciel doit permettre de modifier les attributions initiales via une demande de moyens supplémentaires,
    Et notifier automatiquement les académies concernées des modifications effectuées.
