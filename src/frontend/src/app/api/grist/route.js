// Importation des modules nécessaires
import { GristDocAPI } from 'grist-api';
import { NextResponse } from 'next/server';

// Récupération des variables d'environnement
const GRIST_API_BASE_URL = process.env.GRIST_API_BASE_URL;
const GRIST_API_KEY = process.env.GRIST_API_KEY;
const GRIST_DOC_ID = process.env.GRIST_DOC_ID;

// Vérification de la présence de toutes les variables d'environnement nécessaires
if (!GRIST_API_BASE_URL || !GRIST_API_KEY || !GRIST_DOC_ID) {
  throw new Error('Variables d\'environnement manquantes pour la configuration de Grist');
}

// Initialisation du client Grist avec l'URL complète et la clé API
const gristClient = new GristDocAPI(`${GRIST_API_BASE_URL}/${GRIST_DOC_ID}`, {
  apiKey: GRIST_API_KEY,
});

/**
 * Fonction utilitaire pour gérer les erreurs de manière centralisée
 * @param {Error} error - L'objet erreur capturé
 * @param {string} customMessage - Un message d'erreur personnalisé
 * @returns {NextResponse} Une réponse JSON avec le message d'erreur et un statut 500
 */
function handleError(error, customMessage) {
  console.error(customMessage, error);
  return NextResponse.json({ error: customMessage }, { status: 500 });
}

/**
 * Gestionnaire de route GET pour récupérer des données d'une table Grist
 * @param {Request} request - L'objet de requête Next.js
 * @returns {NextResponse} Une réponse JSON avec les données ou un message d'erreur
 */
export async function GET(request) {
    console.log({'obj':request.nextUrl.searchParams});
    
  // Extraction de l'ID de la table depuis les paramètres de la requête
  const tableId = request.nextUrl.searchParams.get('tableId');

  // Vérification de la présence de l'ID de la table
  if (!tableId) {
    return NextResponse.json({ error: 'L\'ID de la table est requis' }, { status: 400 });
  }

  try {
    // Récupération des enregistrements de la table spécifiée
    const records = await gristClient.fetchTable(tableId);
    return NextResponse.json(records);
  } catch (error) {
    // Gestion des erreurs lors de la récupération des données
    return handleError(error, 'Échec de la récupération des données depuis Grist');
  }
}

/**
 * Gestionnaire de route POST pour ajouter un enregistrement à une table Grist
 * @param {Request} request - L'objet de requête Next.js
 * @returns {NextResponse} Une réponse JSON avec le résultat de l'ajout ou un message d'erreur
 */
export async function POST(request) {
  try {
    // Extraction de l'ID de la table et de l'enregistrement depuis le corps de la requête
    const { tableId, record } = await request.json();

    // Vérification de la présence de l'ID de la table et de l'enregistrement
    if (!tableId || !record) {
      return NextResponse.json({ error: 'L\'ID de la table et l\'enregistrement sont requis' }, { status: 400 });
    }

    // Ajout de l'enregistrement à la table spécifiée
    const result = await gristClient.addRecords(tableId, [record]);
    return NextResponse.json(result);
  } catch (error) {
    // Gestion des erreurs lors de l'ajout de l'enregistrement
    return handleError(error, 'Échec de l\'ajout de l\'enregistrement dans Grist');
  }
}

