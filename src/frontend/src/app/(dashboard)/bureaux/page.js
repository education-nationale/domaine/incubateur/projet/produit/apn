import React from 'react';
import FetchData from '../../../components/grist/FetchData';

const Bureaux = () => {
    return (
        <div>
            <FetchData 
                tableId={'Bureaux2'} 
                title={'Bureaux'}/>
        </div>
    );
};

export default Bureaux;