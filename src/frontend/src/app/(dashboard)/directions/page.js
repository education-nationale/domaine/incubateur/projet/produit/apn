import React from 'react';
import FetchData from '../../../components/grist/FetchData';

const Directions = () => {
    return (
        <div>
            <FetchData 
                tableId={'Directions'} 
                title={'Directions'}
                visibleColumns={['id', 'direction']} // Spécifiez les colonnes
                filterableColumns={['direction']}
                //https://components.react-dsfr.codegouv.studio/?path=/docs/components-table--default
                colorVariant="purple-glycine"
            />
        </div>
    );
};

export default Directions;