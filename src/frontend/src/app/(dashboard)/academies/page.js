import React from 'react';
import FetchData from '../../../components/grist/FetchData';
import PostData from '../../../components/grist/PostData';
const Academies = () => {
    return (
        <div>
            <FetchData 
                tableId='Academies_liste'   
                title='Academies'
                itemsPerPage={15}
                showFilters={false} // Ceci cachera complètement la section des filtres
                visibleColumns={['id', 'identification','code_academie','rectorat']} // Spécifiez les colonnes que vous voulez afficher 
                //https://components.react-dsfr.codegouv.studio/?path=/docs/components-table--default
                colorVariant="green-emeraude"
            />

            <PostData
            tableId="Academies_liste"
            title="Ajouter un nouvel enregistrement"
            fields={[
                { id: 'identification', name: 'identification' },
                { id: 'code_academie', name: 'code_academie' },
                { id: 'rectorat', name: 'rectorat' },
                { id: 'adresse', name: 'adresse' }
            ]}
    
            />
        </div>
    );
};

export default Academies;