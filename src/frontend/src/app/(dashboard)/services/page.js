import React from 'react';
import FetchData from '../../../components/grist/FetchData';

const Services = () => {
    return (
        <div>
            <FetchData 
                tableId={'Services_sous_directions'} 
                title={'Services'}
                itemsPerPage={5}
                visibleColumns={['id', 'nom', 'direction']} // Spécifiez les colonnes que vous voulez afficher
                filterableColumns={['nom','direction']}
                linkedTables={{
                    direction: { tableId: 'Directions', displayColumn: 'direction' },
    
                }}
                //https://components.react-dsfr.codegouv.studio/?path=/docs/components-table--default
                colorVariant="blue-ecume"
            />
        </div>
    );
};

export default Services;