import { DsfrHead } from "@codegouvfr/react-dsfr/next-appdir/DsfrHead";
import { DsfrProvider } from "@codegouvfr/react-dsfr/next-appdir/DsfrProvider";
import { getHtmlAttributes } from "@codegouvfr/react-dsfr/next-appdir/getHtmlAttributes";
import { StartDsfr } from "../components/dsfr/startDsfr";
import { defaultColorScheme } from "../components/dsfr/defaultColorScheme";
import Link from "next/link";
import Header from "../components/Header";

export default function RootLayout({ children }) {
  //NOTE: The lang parameter is optional and defaults to "fr"
  const lang = "fr";
  return (
    
      <html {...getHtmlAttributes({ defaultColorScheme, lang })} >
        
        <head>
          <StartDsfr />
          <DsfrHead Link={Link} />
        </head>
        <body>
            <DsfrProvider lang={lang}>
            
              <Header />
              <div className="fr-container fr-mt-4w">
                <div className="fr-grid-row fr-grid-row--gutters fr-grid-row--center">
                  {children}
                </div>
              </div>
              
        
            </DsfrProvider>
        </body>
        
      </html>
    
  );
}