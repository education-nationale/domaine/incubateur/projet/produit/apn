"use client"

import React, { useState } from 'react'
import { Header as DSFRHeader } from "@codegouvfr/react-dsfr/Header";
import Badge from "@codegouvfr/react-dsfr/Badge";
import { usePathname } from 'next/navigation';

export default function Header() {
  const pathname = usePathname();
  const [activeItem, setActiveItem] = useState(null);

  const handleItemClick = (itemId) => {
    setActiveItem(itemId);
  };

  const navigation = [
    {
      isActive: activeItem === 'active',
      id: 'data',
      text: 'Listes de référence',
      
      menuLinks: [
        {
          linkProps: {
            href: '/academies',
            onClick: () => handleItemClick('active')
          },
          text: 'Académies',
         
        },
        {
          linkProps: {
            href: '/directions',
            onClick: () => handleItemClick('active')
          },
          text: 'Directions',
          
        },
        {
          linkProps: {
            href: '/services',
            onClick: () => handleItemClick('active')
          },
          text: 'Services & sous-directions',
          
        },
        {
          linkProps: {
            href: '/bureaux',
            onClick: () => handleItemClick('active')
          },
          text: 'Bureaux',
          
        },
        {
          linkProps: {
            href: '/operations',
            onClick: () => handleItemClick('active')
          },
          text: 'Opérations',
          
        }
      ],
    },
    
    {
      isActive: activeItem === 'demande' || pathname === '/demande',
      id: 'demande',
      text: 'Formulaire de demande',
      linkProps: {
        href: '/demande',
        onClick: () => handleItemClick('demande'),
      },
    }
  ];

  return (
    <DSFRHeader
      brandTop={<>INTITULE<br />OFFICIEL</>}
      className="fr-mb-3w"
      homeLinkProps={{
        href: '/',
        title: 'Accueil - Nom de l\'entité (ministère, secrétariat d\'état, gouvernement)'
      }}
      id="main-header"
      serviceTitle={<>Action à pilotage national / DGESCO{' '}<Badge as="span" noIcon severity="success">POC</Badge></>}
      navigation={navigation}
    />
  )
}