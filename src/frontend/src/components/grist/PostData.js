'use client'

import { useState } from 'react';
import { Input } from "@codegouvfr/react-dsfr/Input";
import { Select } from "@codegouvfr/react-dsfr/Select";
import { Button } from "@codegouvfr/react-dsfr/Button";
import { Alert } from "@codegouvfr/react-dsfr/Alert";

/**
 * GristDataForm - Composant React pour ajouter des données à une table Grist
 * 
 * @param {Object} props
 * @param {string} props.tableId - L'identifiant de la table Grist
 * @param {string} props.title - Le titre du formulaire
 * @param {Array} props.fields - Liste des champs de la table
 * @param {Object} [props.linkedTables] - Configuration des tables liées
 */
export default function PostData({ 
  tableId, 
  title, 
  fields,
  linkedTables = {}
}) {
  const [formData, setFormData] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(false);

  // Gestion du changement des champs du formulaire
  const handleInputChange = (field, value) => {
    setFormData(prev => ({ ...prev, [field]: value }));
  };

  // Soumission du formulaire
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setError(null);
    setSuccess(false);

    try {
      const response = await fetch('/api/grist', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ tableId, record: formData }),
      });

      if (!response.ok) {
        throw new Error('Erreur lors de l\'ajout de l\'enregistrement');
      }

      setSuccess(true);
      setFormData({}); // Réinitialiser le formulaire
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  // Rendu des champs du formulaire
  const renderFormField = (field) => {
    if (linkedTables[field.id]) {
      // Champ lié : afficher un select
      return (
        <Select
          key={field.id}
          label={field.name}
          nativeSelectProps={{
            value: formData[field.id] || '',
            onChange: (e) => handleInputChange(field.id, e.target.value)
          }}
        >
          <option value="">Sélectionnez une option</option>
          {linkedTables[field.id].options.map(option => (
            <option key={option.id} value={option.id}>
              {option[linkedTables[field.id].displayColumn]}
            </option>
          ))}
        </Select>
      );
    } else {
      // Champ standard : afficher un input
      return (
        <Input
          key={field.id}
          label={field.name}
          value={formData[field.id] || ''}
          onChange={(e) => handleInputChange(field.id, e.target.value)}
        />
      );
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <h1>{title}</h1>
      {fields.map(renderFormField)}
      <Button type="submit" disabled={loading}>
        {loading ? 'Envoi en cours...' : 'Ajouter'}
      </Button>
      {error && <Alert type="error" title="Erreur" description={error} />}
      {success && <Alert type="success" title="Succès" description="Données ajoutées avec succès" />}
    </form>
  );
}