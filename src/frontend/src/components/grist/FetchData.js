'use client'

import { useState, useEffect, useMemo } from 'react';
import { Table } from "@codegouvfr/react-dsfr/Table";
import { Input } from "@codegouvfr/react-dsfr/Input";
import { Pagination } from "@codegouvfr/react-dsfr/Pagination";

/**
 * GristDataTable - Composant React pour afficher des données Grist avec gestion des liens entre tables
 *
 * @param {Object} props
 * @param {number|string} props.tableId - L'identifiant de la table Grist principale
 * @param {string} props.title - Le titre à afficher au-dessus du tableau
 * @param {number} [props.itemsPerPage=10] - Nombre d'éléments à afficher par page
 * @param {string[]} [props.visibleColumns=null] - Liste des colonnes à afficher
 * @param {string[]} [props.filterableColumns=null] - Liste des colonnes filtrables
 * @param {boolean} [props.showFilters=true] - Indique si les champs de filtrage doivent être affichés
 * @param {Object} [props.linkedTables] - Configuration des tables liées (ex: { columnName: { tableId: 'linkedTableId', displayColumn: 'nameColumn' } })
 */
export default function FetchData({
  tableId,
  title,
  itemsPerPage = 10,
  visibleColumns = null,
  filterableColumns = null,
  showFilters = true,
  linkedTables = {},
  colorVariant
}) {
  // États pour gérer les données et l'interface utilisateur
  const [data, setData] = useState([]); // Données de la table principale
  const [loading, setLoading] = useState(true); // État de chargement
  const [error, setError] = useState(null); // Gestion des erreurs
  const [sortConfig, setSortConfig] = useState({ key: null, direction: 'ascending' }); // Configuration du tri
  const [filters, setFilters] = useState({}); // Filtres appliqués
  const [currentPage, setCurrentPage] = useState(1); // Page actuelle pour la pagination
  const [linkedData, setLinkedData] = useState({}); // Données des tables liées

  // Effet pour charger les données au montage du composant ou lorsque tableId change
  useEffect(() => {
    async function fetchData() {
      try {
        // Récupération des données de la table principale
        const mainResponse = await fetch(`/api/grist?tableId=${tableId}`);
        if (!mainResponse.ok) {
          throw new Error('Erreur lors de la récupération des données principales');
        }
        const mainResult = await mainResponse.json();
        setData(mainResult);

        // Récupération des données des tables liées
        const linkedDataPromises = Object.entries(linkedTables).map(async ([columnName, config]) => {
          const linkedResponse = await fetch(`/api/grist?tableId=${config.tableId}`);
          if (!linkedResponse.ok) {
            throw new Error(`Erreur lors de la récupération des données liées pour ${columnName}`);
          }
          const linkedResult = await linkedResponse.json();
          return [columnName, linkedResult];
        });

        const linkedResults = await Promise.all(linkedDataPromises);
        const linkedDataObject = Object.fromEntries(linkedResults);
        setLinkedData(linkedDataObject);

        setLoading(false);
      } catch (err) {
        setError(err);
        setLoading(false);
      }
    }
    fetchData();
  }, []);

  // Détermination des colonnes à afficher et à filtrer
  const allColumns = data.length > 0 ? Object.keys(data[0]) : [];
  const columns = visibleColumns ? allColumns.filter(col => visibleColumns.includes(col)) : allColumns;
  const filtersToShow = filterableColumns ? columns.filter(col => filterableColumns.includes(col)) : columns;

  /**
   * Gère le tri des données lorsqu'un en-tête de colonne est cliqué
   * @param {string} key - La clé (nom de colonne) sur laquelle trier
   */
  const requestSort = (key) => {
    let direction = 'ascending';
    if (sortConfig.key === key && sortConfig.direction === 'ascending') {
      direction = 'descending';
    }
    setSortConfig({ key, direction });
  };

  /**
   * Gère le changement des filtres
   * @param {string} column - Le nom de la colonne à filtrer
   * @param {string} value - La valeur du filtre
   */
  const handleFilterChange = (column, value) => {
    setFilters(prev => ({ ...prev, [column]: value }));
    setCurrentPage(1); // Réinitialise la pagination lors du filtrage
  };

  // Mémo pour trier et filtrer les données
  const sortedAndFilteredData = useMemo(() => {
    let processedData = [...data];

    // Application des filtres
    Object.keys(filters).forEach(column => {
      if (filtersToShow.includes(column)) {
        const filterValue = filters[column].toLowerCase();
        processedData = processedData.filter(item => {
          // Gestion spéciale pour les colonnes liées
          const cellValue = linkedTables[column]
            ? String(linkedData[column]?.find(linked => linked.id === item[column])?.[linkedTables[column].displayColumn] || '')
            : String(item[column]);
          return cellValue.toLowerCase().includes(filterValue);
        });
      }
    });

    // Application du tri
    if (sortConfig.key !== null && columns.includes(sortConfig.key)) {
      processedData.sort((a, b) => {
        // Gestion spéciale pour les colonnes liées lors du tri
        const aValue = linkedTables[sortConfig.key]
          ? linkedData[sortConfig.key]?.find(linked => linked.id === a[sortConfig.key])?.[linkedTables[sortConfig.key].displayColumn]
          : a[sortConfig.key];
        const bValue = linkedTables[sortConfig.key]
          ? linkedData[sortConfig.key]?.find(linked => linked.id === b[sortConfig.key])?.[linkedTables[sortConfig.key].displayColumn]
          : b[sortConfig.key];

        if (aValue < bValue) {
          return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if (aValue > bValue) {
          return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
      });
    }

    return processedData;
  }, [data, sortConfig, filters, columns, filtersToShow, linkedData, linkedTables]);

  // Mémo pour paginer les données
  const paginatedData = useMemo(() => {
    const startIndex = (currentPage - 1) * itemsPerPage;
    return sortedAndFilteredData.slice(startIndex, startIndex + itemsPerPage);
  }, [sortedAndFilteredData, currentPage, itemsPerPage]);

  // Calcul du nombre total de pages
  const pageCount = Math.ceil(sortedAndFilteredData.length / itemsPerPage);

  // Gestion des états de chargement et d'erreur
  if (loading) return <p>Chargement...</p>;
  if (error) return <p>Erreur : {error.message}</p>;
  if (data.length === 0) return <p>Aucune donnée disponible</p>;

  // Rendu du composant
  return (
    <>
      <h1>{title}</h1>
      {/* Affichage conditionnel des champs de filtrage */}
      {showFilters && (
        <div style={{ marginBottom: '1rem' }} className='fr-grid-row'>
          {filtersToShow.map(column => (
            <Input
              key={`filter-${column}`}
              label={`Filtre ${column}`}
              onChange={(e) => handleFilterChange(column, e.target.value)}
              className='fr-col-12 fr-col-md-4 fr-px-1v'
            />
          ))}
        </div>
      )}
      {/* Tableau de données */}
      <div className="fr-grid-row fr-grid-row--gutters fr-grid-row--center">
        <Table
          fixed
          caption={`Tableau des données par ${title.toLowerCase()}`}
          colorVariant={colorVariant}
          headers={columns.map(column => (
            <button
              key={`header-${column}`}
              onClick={() => requestSort(column)}
              aria-sort={sortConfig.key === column ? sortConfig.direction : 'none'}
              className="fr-btn fr-btn--tertiary fr-btn--sm"
            >
              {column}
              {/* Texte pour les lecteurs d'écran */}
              <span className="fr-sr-only">
                {sortConfig.key === column
                  ? (sortConfig.direction === 'ascending' ? 'trié par ordre croissant' : 'trié par ordre décroissant')
                  : 'cliquer pour trier'}
              </span>
              {/* Indicateur visuel de tri */}
              {sortConfig.key === column && (
                <span aria-hidden="true" className="fr-ml-1v">
                  {sortConfig.direction === 'ascending' ? '▲' : '▼'}
                </span>
              )}
            </button>
          ))}
          data={paginatedData.map(record =>
            columns.map(col => {
              // Gestion de l'affichage pour les colonnes liées
              if (linkedTables[col]) {
                const linkedRecord = linkedData[col]?.find(linked => linked.id === record[col]);
                return linkedRecord ? linkedRecord[linkedTables[col].displayColumn] : '';
              }
              return record[col];
            })
          )}
        />
      </div>
      {/* Pagination */}
      {pageCount > 1 && (
        <Pagination
          count={pageCount}
          defaultPage={currentPage}
          onChange={(e, page) => setCurrentPage(page)}
          getPageLinkProps={(page) => ({
            onClick: (e) => {
              e.preventDefault();
              setCurrentPage(page);
            },
            href: `#page-${page}`,
          })}
          className="fr-mt-2w"
        />
      )}
    </>
  )
}
