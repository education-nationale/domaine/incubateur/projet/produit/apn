# Utilise Debian Bookworm comme image de base
FROM debian:bookworm

# Évite les avertissements en passant en mode non-interactif
ENV DEBIAN_FRONTEND=noninteractive

# Installe les paquets nécessaires
RUN apt-get update && apt-get install -y \
    curl \
    git \
    sudo \
    && rm -rf /var/lib/apt/lists/*

# Install Node.js
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get update && apt-get install -y nodejs && rm -rf /var/lib/apt/lists/*

# Crée un utilisateur non-root
ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# Repasse en mode dialogue pour toute utilisation ponctuelle de apt-get
ENV DEBIAN_FRONTEND=dialog

# Définit l'utilisateur par défaut
USER $USERNAME
